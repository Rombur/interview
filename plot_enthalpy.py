#! /usr/bin/env python
#----------------------------------------------------------------------------#
# Python code
# Author: Bruno Turcksin
# Date: 2015-08-24 18:35:31.012740
#----------------------------------------------------------------------------#

import pylab
import numpy as np


temp_1 = np.array([1., 4., 5., 8.])
enth_1 = np.array([1., 2., 8., 10.])
temp_2 = np.array([1., 4.5, 4.5, 8.])
enth_2 = np.array([1., 2., 8., 10.])

pylab.figure(1)
pylab.plot(temp_1, enth_1, color='blue', linewidth=4., linestyle='-')
pylab.plot(temp_2, enth_2, color='red', linewidth=4., linestyle='-')
pylab.xlabel('Temperature', fontsize=20)
pylab.ylabel('Enthalpy', fontsize=20)
pylab.xticks(fontsize=0)
pylab.yticks(fontsize=0)
pylab.savefig('enthalpy.png')

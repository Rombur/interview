\documentclass[notes]{beamer}
%\documentclass{beamer}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{color}
\usepackage{graphicx}
\usepackage{float}
\usepackage{lscape}
\usepackage{multimedia}
\usepackage{enumerate}
\usepackage{hyperref}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{media9}
\usepackage{subcaption}

\usetheme{Frankfurt}

\setbeamertemplate{footline}{
\leavevmode
\hbox{\hspace*{-0.06cm}
\begin{beamercolorbox}[wd=.35\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}
\usebeamerfont{author in head/foot}\insertshortauthor
\end{beamercolorbox}%
\begin{beamercolorbox}[wd=.55\paperwidth,ht=2.25ex,dp=1ex,center]{section in head/foot}
\usebeamerfont{section in head/foot}\insertshorttitle
\end{beamercolorbox}%
\begin{beamercolorbox}[wd=.1\paperwidth,ht=2.25ex,dp=1ex,right]{section in head/foot}
\usebeamerfont{section in head/foot}\insertframenumber{}/\inserttotalframenumber\hspace*{2ex}
\end{beamercolorbox}}}


\newcommand\bn{\boldsymbol{\nabla}}
\newcommand\bo{\boldsymbol{\Omega}}
\newcommand\br{\mathbf{r}}
\newcommand\la{\left\langle}
\newcommand\ra{\right\rangle}
\newcommand\bs{\boldsymbol}
\newcommand\red{\textcolor{red}}
\newcommand\ldb{\{\!\!\{}
\newcommand\rdb{\}\!\!\}}
\newcommand\llb{\llbracket}
\newcommand\rrb{\rrbracket}

\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}
\renewcommand{\[}{\left[}
\renewcommand{\]}{\right]}

\beamertemplatetransparentcovered

\title[Matrix-free AM]{Matrix-free operator evaluation for additive manufacturing
simulation}
\author{Bruno Turcksin}
\institute{Oak Ridge National Laboratory,\\ 
Computer Science and Mathematics Division,\\
Computational Engineering and Energy Sciences Group}
\date{}

\begin{document}

\begin{frame}
  \maketitle
  \note{Additive manufacturing code is in collaboration with Yousub Lee. Work on
  matrix-free for GPU in deal.II is with Karl at Uppsala University in Sweden
  and Martin at Technical University of Munich.}
\end{frame}
%-----------------------------------------------------------------------------
\logo{\includegraphics[height=0.5cm]{logo_ORNL}}
\begin{frame}
  \frametitle{Outline}
  \tableofcontents[hideallsubsections]
\end{frame}
%-----------------------------------------------------------------------------
\section{Matrix-Free}
\begin{frame}
  \frametitle{Outline}
  \tableofcontents[currentsection,sectionstyle=show/hide,
  subsectionstyle=show/show/hide]
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Introduction}
\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
    \item Bandwidth and memory are not improving as quickly as flops; an example
      is Sunway TaihuLight (1.31 PB, 10.6 millions of cores
      $\rightarrow$ 123 MB/core).
    \item In lots of applications, the matrix of the system is only used by the
      Krylov solver to perform SpMV.
    \item SpMV is bandwidth bound on both CPU and GPU.
    \item Matrix-free is advantageous on ``logical'' quadrilaterals and
      hexahedra and for high order FEM. High order is great for convergence and
      vectorization (important for KNL and GPU).
    \item Matrix-free has to be done the right way to minimize computation.
  \end{itemize}
  \note[item]{Remind everybody that this is not related to JFNK.
  Jacobian-free Newton-Krylov methods: a survey of approaches and applications
  Dana Knolls and David Keyes (JCP). They even say that it is not fully matrix-free}
  \note[item]{A naive way to do the matrix-free will require too many
  computation. See next slide}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Matrix-Free}
\begin{frame}
  \frametitle{Matrix-Free}
  \begin{itemize}
    \item $v = A(u) = \sum_{k}^{n cells}P_k^T A_k P_k u$\\
      where $v$ and $u$ are vectors, $A$ is the operator, $P_k$ defines the
      location of the local DoFs in the global vector.
    \item Naive way:
      \begin{enumerate}
        \item Extract local elements: $u_k = P_k u$.
        \item Build $A_k$.
        \item Compute the matrix-vector multiplication: $v_k = A_k u_k$.
        \item Put the local results in the global vector: $v = P_k^T v_k$.
      \end{enumerate}
  \end{itemize}
  \note[item]{$P_k$ can be a rectangular matrix but in practice we can just
  copy/mask some entries.}
  \note[item]{Work on cells is great for thread parallelism + colorization
  $\Rightarrow$ we don't have race conditions when write in the global vector.}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Matrix-Free}
  This naive way is actually very expensive:
  \begin{itemize}
    \item $A_k$ is dense and requires three nested loops.
    \item $A_k u_k$ requires two nested loops.
  \end{itemize}
  \begin{algorithm}[H]
    \caption{Building $A_k$}
    \begin{algorithmic}[1]
      \For {q=1 to n\_quad\_points}
      \For {i=1 to n\_dofs\_per\_cell}
      \For {j=1 to n\_dofs\_per\_cell}
      \State matrix(i,j) $+=$ grad(i,q) $\times$ grad(j,q) $\times$ JxW(q)
      \EndFor
      \EndFor
      \EndFor
    \end{algorithmic}
  \end{algorithm}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Matrix-Free}
  A more efficient way: 
  \begin{itemize}
    \item Rewrite $A_k u_k$ as:
      \begin{equation}
        A_k u_k = B_k^T D_k B_k u_k
      \end{equation}
      where $B_k$ is the matrix formed by grad(i,q)[dim] and $D_k$ is a diagonal
      matrix with dim repeated values of JxW(q).
    \item Perform three MV instead of two MM and one MV $\Rightarrow$ complexity
      goes from $O($n\_dofs\_per\_cell$^3)$ to $O($n\_dofs\_per\_cell$^2)$.
  \end{itemize}
  \note[item]{Interpretation of the algorithm: first transform the vector of
  values on the local DoFs to a vector of gradients on the quadrature points. In
  the second, loop we multiply these gradients by the integration weight. The
  third loop applies the second gradient (transposed), to get back a vector on
  the cell dofs.}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Matrix-Free}
  \begin{itemize}
    \item Bottleneck of the current method is building $B_k$, i.e., computing
      the gradient in the real space.
    \item In general, $B_k$ is different on every cell $\Rightarrow$ too many to
      save them.
    \item Decompose $B_k$ in $B_k = J_k^{-T} B_{ref}$, where $B_{ref}$ is the
      gradient on the reference cell and $J_k^{-T}$ is the inverse transpose
      Jacobian of the transformation from unit to the real cell.
    \item We finally get: $v_k = B_{ref}^T J_k^{-1} D J_k^{-T} B_{ref} u_k$.
  \end{itemize}
  \note[item]{The reinit function computes the gradient in real space by
  transforming the gradient on the reference cell using the Jacobian of the
  transformation from real to reference cell. This is done for each basis
  function on the cell, for each quadrature point. The Jacobian does not depend
  on the basis function, but it is different on different quadrature points in
  general.}
  \note[item]{In deal.II, we check if the cells are simple rectangle and
  identical, in this case we don't do all the computation.}
  \note[item]{deal.II calls the Jacobian matrix from real to unit cell
  inverse\_jacobian, as the forward transformation}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Tensor Product}
\begin{frame}
  \frametitle{Tensor Product}
  One more possible optimization: sum-factorization\\
  1D basis functions: 
  \begin{align}
    f_1 &= x, \\
    f_2 &= 1-x, \\
    g_1 &= y, \\
    g_2 &= 1-y.
  \end{align}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Tensor Product}
  Evaluating the gradient in the $x$ direction: $v = C u$ at the quadrature
  points
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.2\textwidth]{sum_factorization}
  \end{figure}
  \begin{equation}
    C = 
    \begin{pmatrix}
      g_{1A} & g_{1B} \\
      g_{2A} & g_{2B}
    \end{pmatrix}
    \otimes
    \begin{pmatrix}
      f'_{1A} & f'_{1B} \\
      f'_{2A} & f'_{2B}
    \end{pmatrix}
  \end{equation}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Tensor Product}
  \begin{equation}
    \begin{pmatrix}
      v_{11} \\
      v_{12} \\
      v_{21} \\
      v_{22}
    \end{pmatrix}
    =
    \begin{pmatrix}
      g_{1A} f'_{1A} & g_{1A} f'_{1B} & g_{1B} f'_{1A} & g_{1B} f'_{1B} \\
      g_{1A} f'_{2A} & g_{1A} f'_{2B} & g_{1B} f'_{2A} & g_{1B} f'_{2B} \\
      g_{2A} f'_{1A} & g_{2A} f'_{1B} & g_{2B} f'_{1A} & g_{1B} f'_{1B} \\
      g_{2A} f'_{2A} & g_{2A} f'_{2B} & g_{2B} f'_{2A} & g_{2B} f'_{2B}
    \end{pmatrix}
    \begin{pmatrix}
      u_{AA} \\
      u_{AB} \\
      u_{BA} \\
      u_{BB}
    \end{pmatrix}
  \end{equation}
  \begin{itemize}
    \item Computing $Cu$ using a straightforward matrix-vector multiplication
      requires $O(p^4)$ operations.
    \item Can be reduced using tensor product properties $C = G \otimes F = (G
      \otimes I) (I \otimes F)$
    \item $(G \otimes I)$ is composed of diagonal matrices.
    \item $(I \otimes F)$ is block diagonal.
  \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Tensor Product}
  \begin{itemize}
    \item Sum factorization:
      \begin{itemize}
        \item First, computes $w=(I \otimes F)u$:
          \begin{equation}
            w_{ij} = \sum_{k=1}^p f'_{ik} u_{kj}
          \end{equation}
        \item Then, $v = (G \otimes I) w$:
          \begin{equation}
            v_{ij} = \sum_{l=1}^p g_{jl} w_{il} = 
              \sum_{l=1}^p w_{il} g^T_{lj}
          \end{equation}
        \item The total work is $O(2p^3)$
      \end{itemize}
  \end{itemize}
  \note[item]{We see that it becomes more and more advantageous has when p
  increases and for 3D}
  \note[item]{This is a technique from spectral element}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Tensor Product}
  \begin{itemize}
    \item Quadrature points and shape functions are constructed from a tensor
      product of one dimensional object on the reference cell:
      $\frac{\partial}{\partial x_1} u_k(x_1,\hdots,x_n) = \sum_{i_d}
      \phi_{i_d}(x_d) \hdots \sum_{i_2}\phi_{x_2} \sum_{i_1}\phi'_{x1} u^{(i_1,
      \hdots, i_d)} $.
    \item We can evaluate one sum at the time, this can be interpreted as
      applying the basis function one direction at the time.
    \item This reduces the complexity of the product from $p^{2d}$ to $dp^{d+1}$
      where $p$ is the number of shape functions in each direction.
  \end{itemize}
  \note[item]{Tensor product can also be applied to tetrahedral elements but at
  a higher cost.}
  \note[item]{Great effect the higher p is}
  \note[item]{Even better with Gauss-Lobatto quadrature because mass matrix is
  diagonal}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Numerical Results}
\begin{frame}
  \frametitle{Numerical Results}
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{matrix_free}
    \caption{Time per matrix-vector product in seconds per million DoFS for
    different polynomial order (Laplacian on a 3D Cartesian geometry)}
  \end{figure} 
  \footnotetext{A generic interface for parallel cell-based finite element
  operator application, Computer \& Fluids, Kronbichler and Kormann}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Preconditioner}
\begin{frame}
  \frametitle{Preconditioner}
  \begin{itemize}
    \item Matrix is never built $\Rightarrow$ preconditioners such as ILU,
      AMG,... cannot be used.
    \item Need to use:
      \begin{itemize}
        \item matrix-free physics-based preconditioners.
        \item GMG with AMG at the coarsest level: a matrix needs to be build for
          the coarsest level but the system is (hopefully) much smaller.
      \end{itemize}
  \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{GPU}
\begin{frame}
  \frametitle{GPU}
  \begin{itemize}
    \item Similar algorithm than for CPU.
    \item Memory is limited on GPU $\Rightarrow$ not storing the matrix of
      the system is advantageous.
    \item Shown to work for simple (Poisson) problem on complex geometry.
    \item Outperform CUSPARSE for order three and higher (similar performance
      for order two).
    \item Current work is on making the code more general and harmonize the GPU
      and the CPU code $\Rightarrow$ simple to switch from CPU to GPU.
  \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{GPU}
  \begin{itemize}
    \item CPU: parallelize over elements $\Rightarrow$ not enough parallelism
      and memory requirement too high for GPU.
    \item GPU: one thread per DoF but threads cooperate on tensor contraction
      $\Rightarrow$ for low order elements multiple elements are packed into one
      block.
    \item Treatment of hanging nodes is a lot more complicated than on CPU.
  \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\section{Additive Manufacturing}
\begin{frame}
  \frametitle{Outline}
  \tableofcontents[currentsection,sectionstyle=show/hide,
  subsectionstyle=show/show/hide]
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Additive Manufacturing}
\begin{frame}
  \frametitle{Additive Manufacturing}
  \begin{figure}[H]
    \begin{subfigure}[t]{.35\textwidth}
      \centering
      \includegraphics[width=\textwidth]{Inside_Arcam_EBM_A1_System}
      \caption{EBM build chamber}
    \end{subfigure}
    \begin{subfigure}[t]{.35\textwidth}
      \centering
      \includegraphics[width=\textwidth]{schematic-ebm-setup}
      \caption{Schematic setup}
    \end{subfigure}
  \end{figure}
  \footnotetext{http://www.arcam.com/technology/electron-beam-melting/hardware}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Additive Manufacturing}
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{physics}
    \caption{Schematic physics}
  \end{figure}
  \note[item]{Physics: Marangoni effect in the liquid, heat diffusion,
  evaporation, radiative transfer (heat dissipation), charged particle transport
  (electron beam), phase change, crystallization}
  \note[item]{all the material properties are nonlinear}  
  \note[item]{the goal is to send everything into an optimizer with the
  mechanical properties required and get the pathing, speed, power of the beam}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Additive Manufacturing}
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{wine-legs-glass}
    \caption{Marangoni effect in wine}
  \end{figure}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Additive Manufacturing}
  \begin{itemize}
    \item Too many physics to tackle at once $\Rightarrow$ focus on heat
      diffusion and phase change first.
    \item The heat equation without phase change is given by:
      \begin{equation}
        \rho(T) C_p(T) \frac{\partial T}{\partial t} - \bn \cdot \(k\bn T\) = Q,
      \end{equation}
      where $\rho$ is the mass density, $C_p$ is the specific heat, $T$, is the
      temperature, $k$ is the thermal conductivity, and $Q$ is the volumetric heat
      source.
    \item If there are phase changes, the heat equation becomes
      \begin{equation}
        \rho(T) \frac{\partial h(T)}{\partial t} - \bn \cdot \(k\bn T\) = Q,
      \end{equation}
      where $h$ is the enthalpy per unit of mass.
  \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Additive Manufacturing}
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{enthalpy}
    \caption{Enthalpy(T)}
  \end{figure}
  \note[item]{talk about the problem if there are no mushy zone}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Additive manufacturing}
  Electron beam heat source:
  \begin{equation}
    \begin{aligned}
      \dot{Q} &= -\eta_e \eta_b Q_{\max} \frac{4\ln(0.1)}{\pi d^2 z_e}
      e^{\frac{4\ln(0.1) (x^2+y^2)}{s^2}}\(-3\(\frac{z}{z_e}\)^2 -
      2\(\frac{z}{z_e}\) +1\), \\ 
      &= I_e V_e,
    \end{aligned}
  \end{equation}
  where $\eta_e$, $\eta_b$, $d$, $z_e$, $Q_{\max}$, $I_e$, and $V_e$ are resp.
  the energy conversion efficiency on the surface, efficiency of beam control,
  beam diameter, depth where 99\% of the beam is absorbed, electron beam
  current, and acceleration voltage.
  \note[item]{Compton effect: the inelastic scattering of a photon by a charged
  particle}
  \note[item]{Photoelectric effect}
  \note[item]{ionization}
  \note[item]{Show that the machine itself needs to be model}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{AMR}
\begin{frame}
  \frametitle{AMR}
  \begin{itemize}
    \item Thickness of the powder layer $\approx$ 50 $\mu$m.
    \item Objects to manufacture: a few cm to a few meters.
  \end{itemize}
  $\Rightarrow$ cannot use a uniform mesh.\\
  Material is melted by an electron beam $\Rightarrow$ melting zone is very 
  localized and moving over time $\Rightarrow$ cannot create mesh manually 
  $\Rightarrow$ need Adaptive Mesh Refinement (AMR).
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{AMR}
  \begin{itemize}
    \item The mesh needs to be refined:
      \begin{itemize}
        \item where the beam is located: easy because known in advance.
        \item where the material is quickly cooling: need a posteriori error
          estimator.
      \end{itemize}
    \item Need to tune parameters so that the number of unknowns does not explode but 
      the relevant zones are still refined. 
    \item Number of refined cells should change over time: first increase, then 
      depend of the speed of the beam.
  \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Adamantine}
\begin{frame}
  \frametitle{Adamantine}
  Assumptions:
  \begin{itemize}
    \item No movement in the liquid (Marangoni effect).
    \item No evaporation of the material.
    \item No change of volume when the material changes phase.
    \item No loss of heat by radiative transfer.
    \item Material properties are constant per cell and independent of the
      temperature.
    \item Presence of a mushy zone.
  \end{itemize}
  \note[item]{If the change of phase is isothermal, i.e., no mushy zone, the enthalpy
      is discontinuous and belongs to $L^2$. Thus we should use discontinuous
      finite element. However, the temperature is always continuous and belongs to
      $H^1$. We assume that the there is always a mushy zone, so that we can use
      continuous finite element.}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Adamantine}
  \begin{itemize}
    \item Build system, CI (travis + code coverage), and structure of the code
      similar to Cap to improve code reuse.
    \item Use deal.II, p4est, and boost but not Trilinos (no multithreading with
      Epetra and no built-in vectorization operations).
    \item Documentation to install and run the code
      \url{https://rombur.github.io/adamantine/}
    \item Dependencies can be install using spack (package manager developed by
      LLNL).
  \end{itemize}
  \note[item]{spack works on mac, linux desktop and cluster, cray, and will
  support coral machines: sierra, summit, and aurora}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
  \frametitle{Adamantine}
  \begin{itemize}
    \item Unlike Cap, there is no python interface $\Rightarrow$ everything is
      controlled by the input file.
    \item Most inputs accept expressions: $\sin$, $\cos$, rand, if
      statements, etc.
      \begin{block}{Example}
        sources.beam\_0.abscissa "if(t<10, 0.1*t, exp(-t))"
      \end{block}
      if statements can be nested to created more complicated conditions.
  \end{itemize}
  \note[item]{the only thing that is not easily doable is to read from a table.
  Need to do sth like if(t<1, 1, if(t<2, 2, 3))}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Numerical Results}
\begin{frame}
  \frametitle{Numerical Results}
  \begin{itemize}
    \item Two beams in 3D.
    \item One beam in 3D with adaptive mesh refinement.
    \item One beam in 2D with adaptive mesh refinement.
  \end{itemize}
  \note[item]{for now material properties are independent of the temperature and
  we just compute the new state ratio at the end of the time step and go for
  there}
\end{frame}
%-----------------------------------------------------------------------------
\section{Conclusions}
\begin{frame}
  \frametitle{Outline}
  \tableofcontents[currentsection,sectionstyle=show/hide,
  subsectionstyle=show/show/hide]
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Conclusions}
\begin{frame}
  \frametitle{Conclusions}
  \begin{itemize}
    \item Matrix-free is great for new architecture but needs careful
      implementation.
    \item Macroscopic modeling of AM is very complex because it requires
      coupling many different physics.
  \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\subsection{Future Work}
\begin{frame}
  \frametitle{Future Work}
  \begin{itemize}
    \item Phase change needs more testing (so far mainly unit testing).
    \item Structure of the code is similar to Cap $\Rightarrow$ extract
      common parts to improve code reuse.
    \item Material properties should depends on the temperature.
    \item Radiation boundary condition.
    \item Add Marangoni effect.
    \item Add layers of powder.
  \end{itemize}
\end{frame}

\end{document}


#! /usr/bin/env python
#----------------------------------------------------------------------------#
# Python code
# Author: Bruno Turcksin
# Date: 2015-08-24 18:35:31.012740
#----------------------------------------------------------------------------#

import pylab
import numpy as np


degree_mf = np.array([1., 2., 3., 4., 5., 6., 8., 10.])
time_mf = np.array([0.12, 0.05, 0.05, 0.049, 0.056, 0.056, 0.059, 0.069])
time_mf_mpi = np.array([0.015, 0.0072, 0.0076, 0.0072, 0.0087, 0.0087, 0.010,
    0.012])
degree_crs = np.array([1., 2., 3., 4., 5., 6.])
time_crs = np.array([0.047, 0.11, 0.21, 0.41, 0.59, 0.92])
time_crs_mpi = np.array([0.013, 0.028, 0.053, 0.089, 0.14, 0.24])

pylab.figure(1)
pylab.semilogy(degree_mf, time_mf, color='blue', linewidth=4., linestyle='-', 
        marker='D', markersize=10)
pylab.semilogy(degree_mf, time_mf_mpi, color='blue', linewidth=4., linestyle='--', 
        marker='D', markersize=10)
pylab.semilogy(degree_crs, time_crs, color='red', linewidth=4., linestyle='-', 
        marker='D', markersize=10)
pylab.semilogy(degree_crs, time_crs_mpi, color='red', linewidth=4., 
        linestyle='--', marker='D', markersize=10)
pylab.xlabel('Degree of finite element', fontsize=20)
pylab.ylabel('Time per matrix-vector (s/1e6 dofs)', fontsize=20)
pylab.grid(True, which="both")
pylab.legend(['MF', 'MF MPI 8','CRS', 'CRS MPI 8'])
pylab.xticks(fontsize=15)
pylab.yticks(fontsize=15)
pylab.savefig('matrix_free.png')
